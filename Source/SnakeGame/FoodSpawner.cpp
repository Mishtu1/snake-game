// Fill out your copyright notice in the Description page of Project Settings.

#include "FoodSpawner.h"

#include "Food.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
AFoodSpawner::AFoodSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BonusSpawnChance = 0.4f;
}

// Called when the game starts or when spawned
void AFoodSpawner::BeginPlay()
{
	Super::BeginPlay();
	SpawnFood();
}

// Called every frame
void AFoodSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodSpawner::SpawnFood()
{
	auto bIsSpawnBonus = FMath::RandRange(0.f, 1.f) <= BonusSpawnChance;
	TSubclassOf<AFood> FoodClassInt = bIsSpawnBonus ? BonusClass : FoodClass;
	const FVector NewPosition(FMath::RandRange(-425.f, 425.f), FMath::RandRange(-425.f, 425.f), 0);

	FActorSpawnParameters params;
	params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding;	
	CurrentFood = GetWorld()->SpawnActor<AFood>(FoodClassInt, FTransform(NewPosition), params);
	if(CurrentFood)
	{
		CurrentFood->OnEndPlay.AddDynamic(this, &AFoodSpawner::HandleFoodDestroyed);
		return;
	}
	else
		SpawnFood();
		
}

void AFoodSpawner::HandleFoodDestroyed(AActor* Destroyed, EEndPlayReason::Type EndPlayReason)
{
	if (EndPlayReason == EEndPlayReason::Type::Destroyed)
	{
		SpawnFood();
	}
}


